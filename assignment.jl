### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ a7521a67-d175-449e-bbb2-3d14d3ede486
import Pkg

# ╔═╡ aeaa6144-3585-431c-994f-a7d3a8f17658
Pkg.add("RDatasets")

# ╔═╡ e4bc689c-2ed9-452d-8a84-281248b71f6a
Pkg.add("VegaLite")

# ╔═╡ 32dc0ce6-8242-43b6-9460-ae3fe18f0a74
Pkg.add("Plots")

# ╔═╡ c948d4d1-4971-436f-a092-0117c23cc3f6
Pkg.add("StatsBase")

# ╔═╡ 6bcfa792-7024-4e12-ae45-b010b7e46e49
Pkg.add("Statistics")

# ╔═╡ 58d213d0-9b13-44a0-a991-c66f6026760f
Pkg.add("PlutoUI")

# ╔═╡ 2c582473-5f59-46f0-98ba-243cb44230d4
using CSV

# ╔═╡ 932430cd-a3f4-41cc-a8d2-a9a68e388ba5
using Plots

# ╔═╡ 04084c06-bd03-45bf-8f31-c17f36d4246c
using DataFrames

# ╔═╡ 2f22924e-af4b-4e32-bf01-49fea347566c
using StatsBase

# ╔═╡ f8ab18aa-ae52-4d94-8821-c500cc4dee10
using VegaLite

# ╔═╡ d76cabb3-86f4-48a0-96d0-7dd85f996d0d
using Statistics

# ╔═╡ 4404e841-3594-4151-8c3b-ab3a3c38c89b
using PlutoUI

# ╔═╡ f855a81f-e119-471b-898c-c6199b86bc8c
md"## Implementin a linear regression"

# ╔═╡ 2923709b-e968-40e6-b6d7-8feaf6f5685d
real_estate=CSV.read("C:\\Users\\User\\Desktop\\real_estate.csv",DataFrame)

# ╔═╡ 0c05a8a5-e358-4646-9fd9-b31f49c5dd25
begin
	rename!(real_estate,Symbol.(replace.(string.(names(real_estate)),Ref(r"\[m\]"=>"_"))))
	rename!(real_estate,Symbol.(replace.(string.(names(real_estate)),Ref(r"\[s\]"=>"_"))))
	rename!(real_estate,Symbol.(replace.(string.(names(real_estate)),Ref(r"\s"=>"_"))))
end

# ╔═╡ 37926083-da0c-4af1-8768-2935d671ddcc
md"## Data exploration"

# ╔═╡ 874ccbe7-08d7-45c5-acc3-1a2847d763ff
size(real_estate)

# ╔═╡ 41e769af-895f-4342-ae61-8a93e2cf48e4
with_terminal() do
	for col_name in names(real_estate)
		println(col_name)
	end
end

# ╔═╡ daed3b59-11ed-4e25-addd-06ef338d38fc
first(real_estate, 15)

# ╔═╡ add3b6b0-0731-4efe-b43c-90c56cc4faa9
describe(real_estate)

# ╔═╡ 5b3aa570-1b7a-40a9-a300-8c1862b23bfa
with_terminal() do
	describe(real_estate[!, :X1_transaction_date])
end

# ╔═╡ a1d3b78a-f047-492f-8389-59a869f282d5
with_terminal() do
	describe(real_estate[!, :X2_house_age])
end

# ╔═╡ fcaa5829-629b-48ea-941e-0ae94f457266
with_terminal() do
	describe(real_estate[!, :X3_distance_to_the_nearest_MRT_station])
end

# ╔═╡ cb6447b5-e92e-447e-b99e-2835331fd00b
with_terminal() do
	describe(real_estate[!, :X4_number_of_convenience_stores])
end

# ╔═╡ f2427cbd-0839-4915-81dd-d7da9d387675
with_terminal() do
	describe(real_estate[!, :X5_latitude])
end

# ╔═╡ 5c4f7a78-6b0b-4cf1-8d61-892af54034ca
wiAth_terminal() do
	describe(real_estate[!, :X6_longitude])
end

# ╔═╡ 05118a79-20fa-405c-bb67-f8ba3b7d7f8e
with_terminal() do
	describe(real_estate[!, :Y_house_price_of_unit_area])
end

# ╔═╡ 6c1700c7-5f0f-4e63-b1b0-ad8a5375c01c
cor(Matrix(real_estate))

# ╔═╡ c5163c77-11fa-4c46-a40a-4f5e7838fbe5
md"## Building a Model"

# ╔═╡ e8932605-3c4a-46e8-a606-a80b8957b56c
X = real_estate[:, 3:7]

# ╔═╡ 21328b59-36dd-47d4-8f2d-de7ec468bef4
X_mat = Matrix(X)

# ╔═╡ 846bd6f0-6792-426e-8a75-8e60453fb112
Y = real_estate[:, 8]

# ╔═╡ 61d69b7b-2ea2-4025-b1b0-585a90dd596a
Y_mat = Vector(Y)

# ╔═╡ 8fce9ee0-a1b8-4eec-bb93-5d360f9975e3
training_size = 0.7

# ╔═╡ cd818044-1463-4b38-982b-2c50daf14d55
all_data_size = size(X_mat)[1]

# ╔═╡ b99da9ee-ff4c-47d8-a51b-3bb30d5d6571
training_index = trunc(Int, training_size * all_data_size)

# ╔═╡ 8c805ed8-24f0-44f0-a492-c59b6a4e16d0
X_mat_training = X_mat[1: training_index, :]

# ╔═╡ ef0af40b-c895-4d6a-ab32-7d57c98462fb
X_mat_test = X_mat[training_index + 1:end, :]

# ╔═╡ c8ca5e70-08d3-4fbc-95f0-15feb1704c63
Y_mat_train = Y_mat[1:training_index]

# ╔═╡ 7fc29b19-2621-4fcd-bc0f-da4fd936f68c
Y_mat_test = Y_mat[training_index + 1:end]

# ╔═╡ 3eaf5819-2304-4a76-b10c-f9a91a77ca7a
function get_loss(features::Matrix{Float64}, outcome::Vector{Float64}, weight::Vector{Float64})::Float64
	m = size(features)[1]
	hypothesis = features * weights
	loss = hypothesis - outcome
	cost = (1/(2m)) * (loss * loss)
	return cost
end

# ╔═╡ cb5664e2-5ba2-4f60-bdef-c2e6d36f7094
begin
function get_scaling_params(init_features::Matrix{Float64})::Tuple{Matrix{Float64}, Matrix{Float64}}
	feature_mean = mean(init_features, dims=1)
	f_dev = std(init_features, dims=1)
	return (feature_mean, f_dev)
	end
end

# ╔═╡ 47c0775a-b530-4ccb-86c4-545c7f5c92ee
function scale_features(features::Matrix{Float64}, sc_params::Tuple{Matrix{Float64}, Matrix{Float64}})::Matrix{Float64}
	normalised_features = (features .- sc_params[1]) ./ sc_params[2]
end

# ╔═╡ b0e271d8-451d-456e-88ee-96b412a3d3d0
scaling_params = get_scaling_params(X_mat_training)

# ╔═╡ 186fbb04-7cf6-4c19-bb71-e241c31ffa89
scaling_params[1]

# ╔═╡ b4c66f7a-0d5e-459b-80e8-f8e213a1d606
last(scaling_params)

# ╔═╡ 241433e3-a14e-42cc-8761-c0b3d9a791e2
scaled_training_features = scale_features(X_mat_training, scaling_params)

# ╔═╡ bb999ac4-1d50-4d53-a0a3-6d466903139d
scaled_testing_features = scale_features(X_mat_test, scaling_params)

# ╔═╡ ab443dd9-971d-42f4-8d86-02841ad7a1b1
function train_model(features::Matrix{Float64}, outcome::Vector{Float64},alpha::Float64, n_iter::Int64)::Tuple{Vector{Float64},Vector{Float64}}
	total_entry_count = length(outcome)
	aug_features = hcat(ones(total_entry_count, 1), features)
	feature_count = size(aug_features)[2]
	weights = zeros(feature_count)
	loss_vals = zeros(n_iter)

	for i in range(1, stop=n_iter)
		pred = aug_features * weights
		weights = weights - ((alpha/total_entry_count) * aug_features') * (pred - outcome)
	end
	return (weights, loss_vals)
end
	

# ╔═╡ 2f040bc0-af62-452d-b9d0-9801bbb16834
weights_tr_errors = train_model(scaled_training_features, Y_mat_train, 0.03,4000)

# ╔═╡ 658f55ed-cc66-4669-8c02-8497a8eab8ab
plot(weights_tr_errors[2],
label="Cost",
ylabel="Cost",
xlabel="Number of Iteration",
title="Codt Per Iteration")

# ╔═╡ b1f4a5a3-2698-4795-a921-0cc0e71a03e8
@vlplot(:line, x=1:length(weights_tr_errors[2]), y=weights_tr_errors[2])

# ╔═╡ 1aa3b36d-7a5a-44ad-9ce2-80897046aec4
function get_predictions(features::Matrix{Float64},
weights::Vector{Float64})::Vector{Float64}
	total_entry_count = size(features)[1]
	aug_features = hcat(ones(total_entry_count, 1), features)
	preds = aug_features * weights
	return preds
end

# ╔═╡ 8e2168c5-7f5d-49a4-9baa-0a5a9f860a49
function root_mean_square_error(actual_outcome::Vector{Float64},predicted_outcome::Vector{Float64})::Float64
	errors = predicted_outcome - actual_outcome
	squared_errors = errors .^ 2
	mean_squared_errors = mean(squared_errors)
	rmse = sqrt(mean_squared_errors)
	return rmse
end

# ╔═╡ e77fd92b-10ff-4796-8121-f5a87e2d8cfb
training_predictions = get_predictions(scaled_training_features, weights_tr_errors[1])

# ╔═╡ 94b31f2d-b6e3-4441-8865-7446e6008e60
testing_predictions = get_predictions(scaled_testing_features, weights_tr_errors[1])

# ╔═╡ 570a8c5c-9167-4d80-8109-b4d643ab85ca
root_mean_square_error(Y_mat_train, training_predictions)

# ╔═╡ 04493d16-d2df-4f41-9568-c56c61ca18b4
root_mean_square_error(Y_mat_test, testing_predictions)

# ╔═╡ 2207f5cd-6962-4c2f-aa15-ae66a79f8e98
md"## REGULARIZATION"

# ╔═╡ Cell order:
# ╠═f855a81f-e119-471b-898c-c6199b86bc8c
# ╠═a7521a67-d175-449e-bbb2-3d14d3ede486
# ╠═aeaa6144-3585-431c-994f-a7d3a8f17658
# ╠═e4bc689c-2ed9-452d-8a84-281248b71f6a
# ╠═32dc0ce6-8242-43b6-9460-ae3fe18f0a74
# ╠═c948d4d1-4971-436f-a092-0117c23cc3f6
# ╠═6bcfa792-7024-4e12-ae45-b010b7e46e49
# ╠═58d213d0-9b13-44a0-a991-c66f6026760f
# ╠═2c582473-5f59-46f0-98ba-243cb44230d4
# ╠═932430cd-a3f4-41cc-a8d2-a9a68e388ba5
# ╠═04084c06-bd03-45bf-8f31-c17f36d4246c
# ╠═2f22924e-af4b-4e32-bf01-49fea347566c
# ╠═f8ab18aa-ae52-4d94-8821-c500cc4dee10
# ╠═d76cabb3-86f4-48a0-96d0-7dd85f996d0d
# ╠═4404e841-3594-4151-8c3b-ab3a3c38c89b
# ╠═2923709b-e968-40e6-b6d7-8feaf6f5685d
# ╠═0c05a8a5-e358-4646-9fd9-b31f49c5dd25
# ╠═37926083-da0c-4af1-8768-2935d671ddcc
# ╠═874ccbe7-08d7-45c5-acc3-1a2847d763ff
# ╠═41e769af-895f-4342-ae61-8a93e2cf48e4
# ╠═daed3b59-11ed-4e25-addd-06ef338d38fc
# ╠═add3b6b0-0731-4efe-b43c-90c56cc4faa9
# ╠═5b3aa570-1b7a-40a9-a300-8c1862b23bfa
# ╠═a1d3b78a-f047-492f-8389-59a869f282d5
# ╠═fcaa5829-629b-48ea-941e-0ae94f457266
# ╠═cb6447b5-e92e-447e-b99e-2835331fd00b
# ╠═f2427cbd-0839-4915-81dd-d7da9d387675
# ╠═5c4f7a78-6b0b-4cf1-8d61-892af54034ca
# ╠═05118a79-20fa-405c-bb67-f8ba3b7d7f8e
# ╠═6c1700c7-5f0f-4e63-b1b0-ad8a5375c01c
# ╠═c5163c77-11fa-4c46-a40a-4f5e7838fbe5
# ╠═e8932605-3c4a-46e8-a606-a80b8957b56c
# ╠═21328b59-36dd-47d4-8f2d-de7ec468bef4
# ╠═846bd6f0-6792-426e-8a75-8e60453fb112
# ╠═61d69b7b-2ea2-4025-b1b0-585a90dd596a
# ╠═8fce9ee0-a1b8-4eec-bb93-5d360f9975e3
# ╠═cd818044-1463-4b38-982b-2c50daf14d55
# ╠═b99da9ee-ff4c-47d8-a51b-3bb30d5d6571
# ╠═8c805ed8-24f0-44f0-a492-c59b6a4e16d0
# ╠═ef0af40b-c895-4d6a-ab32-7d57c98462fb
# ╠═c8ca5e70-08d3-4fbc-95f0-15feb1704c63
# ╠═7fc29b19-2621-4fcd-bc0f-da4fd936f68c
# ╠═3eaf5819-2304-4a76-b10c-f9a91a77ca7a
# ╠═cb5664e2-5ba2-4f60-bdef-c2e6d36f7094
# ╠═47c0775a-b530-4ccb-86c4-545c7f5c92ee
# ╠═b0e271d8-451d-456e-88ee-96b412a3d3d0
# ╠═186fbb04-7cf6-4c19-bb71-e241c31ffa89
# ╠═b4c66f7a-0d5e-459b-80e8-f8e213a1d606
# ╠═241433e3-a14e-42cc-8761-c0b3d9a791e2
# ╠═bb999ac4-1d50-4d53-a0a3-6d466903139d
# ╠═ab443dd9-971d-42f4-8d86-02841ad7a1b1
# ╠═2f040bc0-af62-452d-b9d0-9801bbb16834
# ╠═658f55ed-cc66-4669-8c02-8497a8eab8ab
# ╠═b1f4a5a3-2698-4795-a921-0cc0e71a03e8
# ╠═1aa3b36d-7a5a-44ad-9ce2-80897046aec4
# ╠═8e2168c5-7f5d-49a4-9baa-0a5a9f860a49
# ╠═e77fd92b-10ff-4796-8121-f5a87e2d8cfb
# ╠═94b31f2d-b6e3-4441-8865-7446e6008e60
# ╠═570a8c5c-9167-4d80-8109-b4d643ab85ca
# ╠═04493d16-d2df-4f41-9568-c56c61ca18b4
# ╠═2207f5cd-6962-4c2f-aa15-ae66a79f8e98
